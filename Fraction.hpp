//
//  Fraction.hpp
//  interview
//
//  Created by Chae Ho Cho on 10/05/2019.
//  Copyright © 2019 Chae Ho Cho. All rights reserved.
//

#ifndef Fraction_hpp
#define Fraction_hpp

#include <stdio.h>
#include <iostream>

class CFraction
{
public:
    CFraction(int numerator, int denominator);
    CFraction(int numerator );
    CFraction(std::string str);
    CFraction();
    
    int getNumerator ( void ) const
    {
        return mNumerator;
    }
    
    int getDenominator ( void ) const
    {
        return mDenominator;
    }
    
    double getDoubleValue ( void )
    {
        return (double)mNumerator/(double)mDenominator;
    }
    
    friend std::ostream &operator<< (std::ostream &os, const CFraction &fraction);

    friend CFraction operator*(const int &intvalue, const CFraction &rhs);

    CFraction operator*(const int &intvalue);
    CFraction operator*(const CFraction &fraction);

    CFraction& operator*=(const int &intvalue);
    CFraction& operator*=(const CFraction &fraction);
    
    void Reduce();
    
private:
    int mNumerator;  // the top number of a fraction
    int mDenominator; // the bottom number of a fraction
    int getGCD(int numerator, int denominator);
};

#endif /* Fraction_hpp */
