//
//  Fraction.cpp
//  interview
//
//  Created by Chae Ho Cho on 10/05/2019.
//  Copyright © 2019 Chae Ho Cho. All rights reserved.
//

#include "Fraction.hpp"

std::ostream &operator<< (std::ostream &os, const CFraction &fraction)
{
    if ( fraction.getDenominator() == 0 )
        return os << "undefined";
    else if ( fraction.getDenominator() == 1 )
        return os << fraction.getNumerator();
    else
        return os << fraction.getNumerator() << '/' << fraction.getDenominator();
}

CFraction operator*(const int &intvalue, const CFraction &rhs)
{
    CFraction tmp( intvalue );
    CFraction tmp2 = tmp * rhs;
    return tmp2;
}

CFraction::CFraction(std::string str )
{
    unsigned long index = str.find('/', 0);
    if ( index != -1 )
    {
        mNumerator = stoi(str.substr(0,index));
        mDenominator = stoi (str.substr(index+1, str.length()));
    }
    else
    {
        mNumerator = stoi ( str );
    }
    
    if ( mNumerator < 0 && mDenominator < 0 )
    {
        mNumerator *= -1;
        mDenominator *= -1;
    }
    else if ( mNumerator > 0 && mDenominator < 0 ) // numerator only has - value
    {
        mNumerator *= -1;
        mDenominator *= -1;
    }
}


CFraction::CFraction(int numerator, int denominator)
{
    if ( numerator < 0 && denominator < 0 )
    {
        numerator *= -1;
        denominator *= -1;
    }
    else if ( numerator > 0 && denominator < 0 ) // numerator only has - value
    {
        numerator *= -1;
        denominator *= -1;
    }
    mNumerator = numerator;
    mDenominator = denominator;
}

CFraction::CFraction(int numerator )
{
    mNumerator = numerator;
    mDenominator = 1;
}

CFraction::CFraction()
{
    mNumerator = 0;
    mDenominator = 1;
}


CFraction CFraction::operator*(const int &intvalue)
{
    CFraction temp ( intvalue );
    temp = *this * temp;
    return temp;
}

CFraction CFraction::operator*(const CFraction &fraction)
{
    int numerator = fraction.getNumerator();
    int denominator = fraction.getDenominator();

    CFraction temp (mNumerator * numerator, mDenominator * denominator);
    temp.Reduce();
    return temp;
}

CFraction& CFraction::operator*=(const int &intvalue)
{
    *this = *this * intvalue;
    return *this;
}

CFraction& CFraction::operator*=(const CFraction &fraction)
{
    *this = *this * fraction;
    return *this;
}

int CFraction::getGCD(int numerator, int denominator)
{
    if ( denominator == 0 )
    {
        return numerator;
    }
    if ( numerator < denominator  )
    {
        return getGCD ( denominator, numerator );
    }
    return getGCD ( numerator % denominator, denominator );
}

void CFraction::Reduce()
{
    int gcd = getGCD ( abs(mNumerator), abs(mDenominator) );
    mNumerator = mNumerator/gcd;
    mDenominator = mDenominator/gcd;
}
