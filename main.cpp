/* C program for insertion sort on a linked list */
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <iostream>
#include "Fraction.hpp"
using namespace std;

// Driver program to test above functions
int main()
{
    CFraction value1 ( 1, -4 );
    CFraction ret;
    CFraction ret2("-2/-5");
    
    std::cout << "Fraction Test : Fraction * integer" << endl;
    std::cout << value1 << " * 5" << " = " << value1 * 5 << endl;
    
    std::cout << "Fraction Test : integer * Fraction" << endl;
    std::cout << "5 * " << ret2 << " = " <<  5 * ret2 << endl;

    std::cout << "Fraction Test : Fraction * Fraction" << endl;
    std::cout << value1 << " * " << ret2 << " = " << value1 * ret2 << endl;
    return 0;
}
