OBJ1 := main.o Fraction.o

CFLAGS += -D_GNU_SOURCE -g 


INCLUDE = -I../include
LOCAL_STATIC_LIBRARIES := 

## Main
all: $(OBJ1) 
	$(CXX) ${CFLAGS} ${DEFS} ${INCLUDE} -o fraction $(OBJ1) 

## Compile Module
.c.o:
	$(CC) ${CFLAGS} ${DEFS} ${C_FLAGS} ${INCLUDE} -c -o $@ $<

.cpp.o:
	$(CXX) ${CFLAGS} ${DEFS} ${INCLUDE} -c -o $@ $<

.m.o:
	$(CC) ${CFLAGS} ${DEFS} ${INCLUDE} -c -o $@ $<

## Clear Module
clean:
	/bin/rm -f ${OBJ1}
	/bin/rm -f *.tif*
	/bin/rm -f fraction


