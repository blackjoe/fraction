1. Problem Define
    Multiply two Fraction objects. For ex., F1*F2
    Multiply a Fraction object by an integer.  For ex., F1*3
    Multiply an integer by a Fraction object.  For ex.,  3*F1

2. Problem Approach Method
    Set The Numerator and Denominator
    Seperate Operation between Numerator and Denominator
    Reduce Numerator and Denominator using Greatest Common Divisor
    Using C++ operation overloading

3.Compile Environment 
    OS : Mac OS 
    Apple LLVM version 10.0.1 (clang-1001.0.46.4)
    Target: x86_64-apple-darwin18.2.0

4.Compile Result
    Fraction Test : Fraction * integer
    -1/4 * 5 = -5/4
    Fraction Test : integer * Fraction
    5 * 2/5 = 2
    Fraction Test : Fraction * Fraction
    -1/4 * 2/5 = -1/10
